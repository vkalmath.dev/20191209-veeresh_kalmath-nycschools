package com.vk.nyschools.api

import androidx.test.runner.AndroidJUnit4
import com.vk.nyschools.data.api.SchoolDbService
import com.vk.nyschools.di.SCHOOL_API_BASE_URL
import io.reactivex.Observable
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class ApiTest {

    lateinit var okHttpClient: OkHttpClient
    lateinit var retrofit: Retrofit
    lateinit var svc: SchoolDbService

    @Before
    fun setup() {
        okHttpClient = OkHttpClient.Builder()
            .addInterceptor(
                HttpLoggingInterceptor()
                    .apply {
                        level = HttpLoggingInterceptor.Level.BODY
                    }).build()

        retrofit = Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl(SCHOOL_API_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build();

        svc = retrofit.create(SchoolDbService::class.java)
    }


    @Test
    fun getOneSchool_is_correct() {
        val testObserver = svc.getSchools(1, 0)
//            .flatMap { Observable.fromIterable(it) }
            .test()

        testObserver
            .assertNoErrors()
            .assertValueCount(1)

    }

    @Test
    fun getTenSchools_is_correct() {
        val testObserver = svc.getSchools(10, 0)
            .flatMap { Observable.fromIterable(it) }
            .test()

        testObserver
            .assertNoErrors()
            .assertValueCount(10)

    }

    @Test
    fun getSchoolCount_is_correct() {
        val testObserver = svc.numberOfSchools()
            .flatMap { Observable.fromIterable(it) }
            .test()

        testObserver
            .assertNoErrors()
            .assertValue { schools ->
                schools.count == 440
            }
    }

    @Test
    fun testTakers_for_dbn_01M292_are_29() {
        val testObserver = svc.getSchoolSATDetail("01M292")
            .flatMap { Observable.just(it[0]) }
            .test()

        testObserver
            .assertNoErrors()
            .assertValue { sat ->
                sat.numOfSatTestTakers == 29
            }

    }


    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }
}

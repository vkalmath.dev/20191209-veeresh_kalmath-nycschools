package com.vk.nyschools.db

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.platform.app.InstrumentationRegistry
import com.google.gson.Gson
import com.vk.nyschools.db.school.SchoolDao
import com.vk.nyschools.db.school.SchoolDatabase
import com.vk.nyschools.model.school.School
import com.vk.nyschools.model.school.SchoolsResponse
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.InputStreamReader


@RunWith(AndroidJUnit4ClassRunner::class)
class SchoolDatabaseTest {

    private lateinit var schoolDao: SchoolDao
    private lateinit var db: SchoolDatabase

    @Before
    fun setUp() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(
            context,
            SchoolDatabase::class.java
        )
            .build()
        schoolDao = db.schoolDao()
    }

    @After
    fun tearDown() {
        db.close()
    }


    @Test
    fun verifyDbIsEmpty() {
        val list = schoolDao.listAllSchools()
        assertThat(list.isEmpty(), `is`(true))
    }

    @Test
    fun verifyOneItemInserted() {
        val school = Gson().fromJson<School>(
            InputStreamReader(InstrumentationRegistry.getInstrumentation().context.assets.open("one_school_response.json")),
            School::class.java
        )
        schoolDao.insert(listOf(school))


        assertThat(schoolDao.listAllSchools().size, `is`(1))
        assertThat(schoolDao.listAllSchools().get(0).dbn, `is`("02M260"))
    }


    @Test
    fun verifyTwoItemsInserted() {
        val schoolsResponse = Gson().fromJson<SchoolsResponse>(
            InputStreamReader(InstrumentationRegistry.getInstrumentation().context.assets.open("schools_response.json")),
            SchoolsResponse::class.java
        )
        schoolDao.insert(schoolsResponse.schools!!)


        assertThat(schoolDao.listAllSchools().size, `is`(2))
        assertThat(schoolDao.listAllSchools().get(0).dbn, `is`("02M260"))
        assertThat(schoolDao.listAllSchools().get(1).dbn, `is`("21K728"))
    }


    @Test
    fun verifySchoolByName() {
        val schoolsResponse = Gson().fromJson<SchoolsResponse>(
            InputStreamReader(InstrumentationRegistry.getInstrumentation().context.assets.open("schools_response.json")),
            SchoolsResponse::class.java
        )
        schoolDao.insert(schoolsResponse.schools!!)


        val schools = schoolDao.listSchoolByName("Clinton School Writers & Artists, M.S. 260")

        assertThat(schools.size, `is`(1))
        assertThat(schools.get(0).schoolName, `is`("Clinton School Writers & Artists, M.S. 260"))
    }


    @Test
    fun verifySchoolByDbn() {
        val schoolsResponse = Gson().fromJson<SchoolsResponse>(
            InputStreamReader(InstrumentationRegistry.getInstrumentation().context.assets.open("schools_response.json")),
            SchoolsResponse::class.java
        )
        schoolDao.insert(schoolsResponse.schools!!)


        val testObserver = schoolDao.schoolByDbn("02M260").test()

        testObserver.assertNoErrors()
//            .assertValue { school ->
//                school.dbn == "02M260"
//            }


    }

    @Test
    fun verifySchoolCount() {
        val schoolsResponse = Gson().fromJson<SchoolsResponse>(
            InputStreamReader(InstrumentationRegistry.getInstrumentation().context.assets.open("schools_response.json")),
            SchoolsResponse::class.java
        )
        schoolDao.insert(schoolsResponse.schools!!)


        val count = schoolDao.schoolsCount()

        assertThat(count, `is`(2))
    }


}

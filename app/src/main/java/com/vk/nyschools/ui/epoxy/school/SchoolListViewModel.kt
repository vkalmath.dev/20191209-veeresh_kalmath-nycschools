package com.vk.nyschools.ui.epoxy.school

import android.app.Application
import android.content.Context
import androidx.lifecycle.*
import androidx.paging.PagedList
import com.vk.nyschools.data.SchoolDbRepository
import com.vk.nyschools.model.school.RepoSchoolResult
import com.vk.nyschools.model.school.School
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers


sealed class DetailSchoolViewState

class SchoolError(val error: String) : DetailSchoolViewState()
object SchoolLoading : DetailSchoolViewState()
class SchoolDetail(val school: School) : DetailSchoolViewState()

class SchoolListViewModel constructor(
    application: Context,
    val schoolDbRepository: SchoolDbRepository
) : AndroidViewModel(application as Application) {

    private val compositeDisposable = CompositeDisposable()
    private val queryLiveData = MutableLiveData<Pair<String, Int>>()
    private val repoResult: LiveData<RepoSchoolResult> = Transformations.map(queryLiveData) {
        schoolDbRepository.search(it.first, it.second)
    }

    val repos: LiveData<PagedList<School>> = Transformations.switchMap(repoResult) { it.data }
    val networkErrors: LiveData<String> = Transformations.switchMap(repoResult) {
        it.networkErrors
    }
    val loading: LiveData<Boolean> = Transformations.switchMap(repoResult) {
        it.loading
    }

    init {
        schoolDbRepository.compositeDisposable = compositeDisposable
    }


    fun search(query: String = " ", page: Int = 1) {
        queryLiveData.postValue(Pair(query, page))
    }

    fun getSchoolDetail(dbn: String, updateUi: (DetailSchoolViewState) -> Unit) {
        compositeDisposable.add(
            schoolDbRepository.schoolByDbn(dbn)
                .flatMap { school ->
                    //If SAT data does not exist in table make api call and update the columns for SAT
                    if (school.satCriticalReadingAvgScore == null ||
                        school.satCriticalReadingAvgScore == -1
                    ) {
                        return@flatMap schoolDbRepository.satByDbn(dbn)
                            .doOnNext {
                                schoolDbRepository.updateSchoolWithSat(dbn, it[0])
                            }
                            .flatMap {
                                schoolDbRepository.schoolByDbn(dbn)
                            }
                            .onErrorReturn {
                                school
                            }
                    } else {
                        Observable.just(school)
                    }
                }
                .map<DetailSchoolViewState> { school ->
                    SchoolDetail(school)
                }
                .startWith(SchoolLoading)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableObserver<DetailSchoolViewState>() {
                    override fun onComplete() {

                    }

                    override fun onNext(state: DetailSchoolViewState) {
                        updateUi(state)
                    }

                    override fun onError(e: Throwable) {
                        updateUi(SchoolError(e.localizedMessage))
                    }

                })
        )
    }


    override fun onCleared() {
        compositeDisposable.clear()
        schoolDbRepository.clear()
    }

}


//Viewmodell Factory
class SchoolListViewModelFactory(
    private val application: Context,
    private val repository: SchoolDbRepository
) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(SchoolListViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return SchoolListViewModel(application, repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}

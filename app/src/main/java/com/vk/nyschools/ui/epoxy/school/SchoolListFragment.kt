package com.vk.nyschools.ui.epoxy.school


import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import androidx.paging.PagedList
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.vk.nyschools.NYSchoolsApplication
import com.vk.nyschools.R
import com.vk.nyschools.data.SCHOOL_DATABASE_PAGE_SIZE
import com.vk.nyschools.model.school.School
import com.vk.nyschools.ui.BaseFragment
import com.vk.nyschools.ui.MainActivity
import kotlinx.android.synthetic.main.fragment_list.*
import timber.log.Timber
import javax.inject.Inject

const val BUNDLE_ARG_SCHOOL_ID = "BUNDLE_ARG_SCHOOL_ID"
const val BUNDLE_ARG_SCHOOL_CURRENT_PAGE = "BUNDLE_ARG_SCHOOL_CURRENT_PAGE"
const val SCHOOL_SHARED_PREFS = "SCHOOL_SHARED_PREFS"
const val BUNDLE_ARG_SCHOOL_NAME = "BUNDLE_ARG_SCHOOL_NAME"

class SchoolListFragment : BaseFragment() {

    @Inject
    lateinit var viewModelFactory: SchoolListViewModelFactory
    @Inject
    lateinit var applicationContext: Context
    @Inject
    lateinit var pagedListController: SchoolListEpoxyController
    @Inject
    lateinit var sharedPrefs: SharedPreferences

    private lateinit var schoolListViewModel: SchoolListViewModel

    private var currentPage: Long = 1

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_list, container, false)
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        (activity?.application as NYSchoolsApplication).appComponent.schoolListComponent()
            .create().inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        schoolListViewModel =
            ViewModelProviders.of(activity!!, viewModelFactory).get(SchoolListViewModel::class.java)

        Timber.e(schoolListViewModel.toString())
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        currentPage = activity?.getSharedPreferences(SCHOOL_SHARED_PREFS, 0)
            ?.getLong(BUNDLE_ARG_SCHOOL_CURRENT_PAGE, 1) ?: 1

        Timber.d("currentPage ${currentPage}")

        registerViewmodelLiveDataObservers()

        setupRecyclerView()

        savedInstanceState?.let {

        } ?: run {
            val page = currentPage.toInt()
            schoolListViewModel.search("", page)
        }
    }

    override fun setupToolbar() {
        (activity as MainActivity)?.supportActionBar?.title = getString(R.string.schools_of_newyork)
        (activity as MainActivity)?.supportActionBar?.setDisplayHomeAsUpEnabled(false)
    }

    private fun registerViewmodelLiveDataObservers() {
        schoolListViewModel.repos.observe(this,
            Observer<PagedList<School>> { pagedList ->
                pagedList?.let {
                    pagedListController.submitList(it)
                }
            }
        )

        schoolListViewModel.loading.observe(this, object : Observer<Boolean> {
            override fun onChanged(isLoading: Boolean?) {
                isLoading?.let {
                    pagedListController.isLoading = it
                }

            }

        })

        schoolListViewModel.networkErrors.observe(this, object : Observer<String> {
            override fun onChanged(errorString: String?) {
                errorString?.let {
                    pagedListController.error = it
                }
            }

        })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        schools_rv.adapter = null
    }

    private fun setupRecyclerView() {
        schools_rv.apply {
            layoutManager =
                LinearLayoutManager(applicationContext, RecyclerView.VERTICAL, false)
            adapter = pagedListController.adapter
            addItemDecoration(DividerItemDecoration(context, LinearLayoutManager.VERTICAL))
        }

        pagedListController.clickListener = { model, _, _, _ ->
            val page: Long = model?.let {
                it.primaryId!! / SCHOOL_DATABASE_PAGE_SIZE
            } ?: run {
                1L
            }
            val bundle = Bundle().apply {
                putString(BUNDLE_ARG_SCHOOL_ID, model?.dbn ?: "")
                putSerializable(BUNDLE_ARG_SCHOOL_NAME, model?.title ?: "")
                putLong(BUNDLE_ARG_SCHOOL_CURRENT_PAGE, page)
                sharedPrefs
                    .edit()?.putLong(BUNDLE_ARG_SCHOOL_CURRENT_PAGE, page)
                    ?.apply()
            }

            if (isTablet) {
                activity?.findNavController(R.id.my_nav_host_fragment_tablet)
                    ?.navigate(R.id.action_home_dest_to_detail_dest_tablet, bundle)
            } else {
                activity?.findNavController(R.id.my_nav_host_fragment)
                    ?.navigate(R.id.action_home_dest_to_detail_dest_school, bundle)
            }
        }
    }

}

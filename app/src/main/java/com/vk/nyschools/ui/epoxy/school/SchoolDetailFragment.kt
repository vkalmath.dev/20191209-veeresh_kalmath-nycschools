package com.vk.nyschools.ui.epoxy.school


import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.vk.nyschools.NYSchoolsApplication
import com.vk.nyschools.R
import com.vk.nyschools.ui.BaseFragment
import com.vk.nyschools.ui.MainActivity
import kotlinx.android.synthetic.main.erro_layout.view.*
import kotlinx.android.synthetic.main.school_detail_layout.*
import timber.log.Timber
import javax.inject.Inject
import android.view.MenuItem

/**
 * A simple [Fragment] subclass.
 */
class SchoolDetailFragment : BaseFragment() {

    private lateinit var schoolListViewModel: SchoolListViewModel

    @Inject
    lateinit var viewModelFactory: SchoolListViewModelFactory

    private lateinit var schoolDbn: String

    private lateinit var title: String


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.school_detail_layout, container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (activity?.application as NYSchoolsApplication).appComponent.schoolListComponent()
            .create().inject(this)

    }

    override fun setupToolbar() {
        (activity as MainActivity).supportActionBar?.title = title
        if (!isTablet) {
            (activity as MainActivity).supportActionBar?.setHomeButtonEnabled(true)
            (activity as MainActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                val fm = activity?.supportFragmentManager
                fm?.getBackStackEntryCount()?.let {
                    if (it > 0) {
                        fm.popBackStack()
                        return true
                    }
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        schoolDbn = arguments?.getString(BUNDLE_ARG_SCHOOL_ID) ?: ""
        title = arguments?.getString(BUNDLE_ARG_SCHOOL_NAME) ?: ""

        schoolListViewModel =
            ViewModelProviders.of(this.activity!!, viewModelFactory)
                .get(SchoolListViewModel::class.java)

        Timber.d("schoolDbn passed is ${schoolDbn}")
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        schoolListViewModel.getSchoolDetail(schoolDbn) { state ->
            when (state) {
                is SchoolError -> {
                    error_view?.visibility = View.VISIBLE
                    error_view?.error_tv?.text = state.error
                    Toast.makeText(context, "Error: ${state.error}", Toast.LENGTH_LONG).show()

                    loading_view?.visibility = View.GONE
                }

                SchoolLoading -> {
                    loading_view?.visibility = View.VISIBLE
                    Toast.makeText(context, "Loading School from DB!!", Toast.LENGTH_LONG).show()
                }

                //update UI
                is SchoolDetail -> {
                    school_name?.text = state.school.schoolName ?: ""

                    school_detail?.text = state.school.overviewParagraph
                    test_takers_tv?.text = getString(
                        R.string.sat_test_takers,
                        "${state.school.numOfSatTestTakers ?: "NA"}"
                    )
                    critical_rdng_tv?.text =
                        getString(
                            R.string.reading_avg_score,
                            "${state.school.satCriticalReadingAvgScore ?: "NA"}"
                        )
                    math_avg_tv?.text =
                        getString(R.string.math_avg, "${state.school.satMathAvgScore ?: "NA"}")
                    writing_avg_tv?.text = getString(
                        R.string.writing_avg,
                        "${state.school.satWritingAvgScore ?: "NA"}"
                    )

                    error_view?.visibility = View.GONE
                    loading_view?.visibility = View.GONE
                }
            }
        }
    }
}

package com.vk.nyschools.ui

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.vk.nyschools.R

abstract class BaseFragment : Fragment() {
    val isTablet
        get() = context?.resources?.getBoolean(R.bool.isTablet) ?: false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupToolbar()
    }

    abstract fun setupToolbar()
}

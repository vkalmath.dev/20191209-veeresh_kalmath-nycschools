package com.vk.nyschools.ui.epoxy.school

import android.content.Context
import android.view.View
import android.widget.Toast
import com.airbnb.epoxy.EpoxyModel
import com.airbnb.epoxy.paging.PagedListEpoxyController
import com.vk.nyschools.di.ActivityScope
import com.vk.nyschools.model.school.School
import com.vk.nyschools.ui.epoxy.ErrorEpoxyModel_
import com.vk.nyschools.ui.epoxy.LoadingEpoxyModel_

import timber.log.Timber
import javax.inject.Inject


/**
 * EpoxyController which works with PagedLists
 */
@ActivityScope
class SchoolListEpoxyController @Inject constructor(val context: Context) :
    PagedListEpoxyController<School>() {

    var clickListener: ((model: SchoolItemEpoxyModel?, parent: SchoolItemEpoxyModel.Holder?, cur: View?, pos: Int) -> Unit)? = null

    private var isError: Boolean = false

    var error: String? = ""
        set(value) {
            field = value?.let {
                isError = true
                it
            } ?: run {
                isError = false
                null
            }
            if (isError) {
                requestModelBuild()
            }
        }

    var isLoading = false
        set(value) {
            field = value
            if (field) {
                requestModelBuild()
            }
        }


    /**
     * Create the EpoxyViewModels
     */
    override fun buildItemModel(currentPosition: Int, item: School?): EpoxyModel<*> {
        item?.let {

            //School Item View Model
            return SchoolItemEpoxyModel_()
                .dbn(item.dbn)
                .schholId(item.primary_id)
                .id("school${item.primary_id}")
                .primaryId(item.primary_id?.toLong())
                .title("${item.schoolName ?: "Unknown"}")
                .programs("Sports: ${item.schoolSports ?: "Unknown"}")
                .description(item.academicopportunities1 ?: "Uknown")
                .thumbnailUrl("https://ca-times.brightspotcdn.com/dims4/default/919f462/2147483647/strip/true/crop/5100x3400+0+0/resize/840x560!/quality/90/?url=https%3A%2F%2Fcalifornia-times-brightspot.s3.amazonaws.com%2F46%2F90%2Fa16e696b40f7bf2dc5c2da7d7bbd%2Fla-photos-1staff-464111-me-marincountyschooldistrictreform-7-ajs.JPG")
                .phoneNumber("Phone: ${item.phoneNumber ?: "Not Available"}")
                .rating("Safe: ${(item.pctStuSafe?.toFloat() ?: 1.0f).times(100)}%")
                .clickListener { model, parent, view, position ->
                    Toast.makeText(
                        context,
                        "clicked on position ${position} and ${model.title()}",
                        Toast.LENGTH_LONG
                    ).show()

                    clickListener?.let {
                        it(model, parent, view, position)
                    }
                }

        } ?: run {
            //Loading View Model
            return LoadingEpoxyModel_()
                .id("loading")
        }
    }

    /**
     * Adding models
     */
    override fun addModels(models: List<EpoxyModel<*>>) {
        if (isError) {
            super.addModels(
                models.plus(
                    //Error View Model
                    ErrorEpoxyModel_()
                        .id("Error")
                        .errorStr(error)
                ).filter { !(it is LoadingEpoxyModel_) }
            )
        } else if (isLoading) {
            super.addModels(
                models.plus(
                    //Error View Model
                    LoadingEpoxyModel_()
                        .id("loading")
                ).distinct()
            )
        } else {
            super.addModels(models.distinct())
        }
    }

    override fun onExceptionSwallowed(exception: RuntimeException) {
        Timber.e("inside onExceptionSwallowed: ${exception.localizedMessage}")
    }
}

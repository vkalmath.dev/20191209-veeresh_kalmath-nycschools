package com.vk.nyschools.ui.epoxy.school

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.airbnb.epoxy.*
import com.squareup.picasso.Picasso
import com.vk.nyschools.R

@EpoxyModelClass(layout = R.layout.school_item_layout)
abstract class SchoolItemEpoxyModel : EpoxyModelWithHolder<SchoolItemEpoxyModel.Holder>() {

    @EpoxyAttribute
    var dbn: String? = null
    @EpoxyAttribute
    var schholId: Int? = null
    @EpoxyAttribute
    var primaryId: Long? = null
    @EpoxyAttribute
    lateinit var title: String
    @EpoxyAttribute
    lateinit var description: String
    @EpoxyAttribute
    lateinit var thumbnailUrl: String
    @EpoxyAttribute
    lateinit var clickListener: View.OnClickListener
    @EpoxyAttribute
    lateinit var programs: String
    @EpoxyAttribute
    lateinit var rating: String
    @EpoxyAttribute
    lateinit var phoneNumber: String

    override fun bind(holder: Holder) {
        if (title.equals("loading")) {
            holder.titleView?.text = title
            holder.descriptionView?.visibility = View.GONE
            holder.thumbnailImageView?.visibility = View.GONE
        } else {
            holder.descriptionView?.visibility = View.VISIBLE
            holder.thumbnailImageView?.visibility = View.VISIBLE

            holder.titleView?.text = title
            holder.descriptionView?.text = description
            Picasso.get().load(thumbnailUrl)
                .fit()
                .centerCrop()
                .placeholder(R.color.primary_dark_material_light)
                .into(holder.thumbnailImageView)
            holder.thumbnailImageView?.visibility = View.GONE
            holder.container?.setOnClickListener(clickListener)
            holder.rating?.text = rating
            holder.programs?.text = programs
            holder.phoneNumber?.text = phoneNumber
        }
    }

    class Holder : EpoxyHolder() {
        var titleView: TextView? = null
        var descriptionView: TextView? = null
        var thumbnailImageView: ImageView? = null
        var container: View? = null
        var rating: TextView? = null
        var phoneNumber: TextView? = null
        var programs: TextView? = null

        override fun bindView(itemView: View) {
            titleView = itemView.findViewById(R.id.school_title)
            descriptionView = itemView.findViewById(R.id.school_description)
            thumbnailImageView = itemView.findViewById(R.id.school_thumbnail)
            container = itemView.findViewById(R.id.school_container)
            rating = itemView.findViewById(R.id.pct_stu_safe)
            phoneNumber = itemView.findViewById(R.id.phone_number)
            programs = itemView.findViewById(R.id.school_programs)
        }

    }
}

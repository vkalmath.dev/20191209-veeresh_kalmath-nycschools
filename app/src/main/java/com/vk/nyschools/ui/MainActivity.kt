package com.vk.nyschools.ui

import android.content.SharedPreferences
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import com.vk.nyschools.NYSchoolsApplication
import com.vk.nyschools.R
import com.vk.nyschools.ui.epoxy.school.BUNDLE_ARG_SCHOOL_CURRENT_PAGE
import javax.inject.Inject


class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var prefs: SharedPreferences
    @Inject
    lateinit var app: NYSchoolsApplication

    override fun onCreate(savedInstanceState: Bundle?) {

        (application as NYSchoolsApplication).appComponent.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(com.vk.nyschools.R.id.toolbar))
    }

    override fun onStop() {
        super.onStop()
        prefs.edit()?.putLong(BUNDLE_ARG_SCHOOL_CURRENT_PAGE, 1)
            ?.apply()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                if (resources?.getBoolean(R.bool.isTablet) ?: false) {

                } else {
                    findNavController(R.id.my_nav_host_fragment)
                        .navigateUp()
                }
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}

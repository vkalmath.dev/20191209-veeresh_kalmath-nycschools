package com.vk.nyschools


import android.app.Application
import com.vk.nyschools.di.AppComponent
import com.vk.nyschools.di.DaggerAppComponent
import com.vk.nyschools.ui.epoxy.school.SCHOOL_SHARED_PREFS
import timber.log.Timber


class NYSchoolsApplication : Application() {

    val appComponent: AppComponent by lazy {
        DaggerAppComponent.factory().create(applicationContext, this, getSharedPreferences(
            SCHOOL_SHARED_PREFS, 0))
    }


    override fun onCreate() {
        super.onCreate()
        // This will initialise Timber
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }
}

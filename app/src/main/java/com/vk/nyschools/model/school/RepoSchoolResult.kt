package com.vk.nyschools.model.school

import androidx.lifecycle.LiveData
import androidx.paging.PagedList

data class RepoSchoolResult(
    val data: LiveData<PagedList<School>>,
    val networkErrors: LiveData<String>,
    val loading: LiveData<Boolean>
)

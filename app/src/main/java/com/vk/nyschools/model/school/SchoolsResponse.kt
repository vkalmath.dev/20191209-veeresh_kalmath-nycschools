package com.vk.nyschools.model.school

data class SchoolsResponse (
    var schools: List<School>? = null
)

data class SchoolsCount ( var count: Int = 0)

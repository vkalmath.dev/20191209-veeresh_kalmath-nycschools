package com.vk.nyschools.model.school

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


data class SchoolSAT(

    @SerializedName("dbn")
    @Expose
    var dbn: String? = null,
    @SerializedName("school_name")
    @Expose
    var schoolName: String? = null,
    @SerializedName("num_of_sat_test_takers")
    @Expose
    var numOfSatTestTakers: Int? = null,
    @SerializedName("sat_critical_reading_avg_score")
    @Expose
    var satCriticalReadingAvgScore: Int? = null,
    @SerializedName("sat_math_avg_score")
    @Expose
    var satMathAvgScore: Int? = null,
    @SerializedName("sat_writing_avg_score")
    @Expose
    var satWritingAvgScore: Int? = null
)

package com.vk.nyschools.model.school

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity(tableName = "schools")
data class School (

    //This is generated automatically when insert happens and this used for calculating next page.
    @PrimaryKey(autoGenerate = true)
    var primary_id: Int? = null,
    @SerializedName("dbn")
    @Expose
    var dbn: String? = null,
    @SerializedName("school_name")
    @Expose
    var schoolName: String? = null,
    @SerializedName("boro")
    @Expose
    var boro: String? = null,
    @SerializedName("overview_paragraph")
    @Expose
    var overviewParagraph: String? = null,
    @SerializedName("school_10th_seats")
    @Expose
    var school10thSeats: String? = null,
    @SerializedName("academicopportunities1")
    @Expose
    var academicopportunities1: String? = null,
    @SerializedName("academicopportunities2")
    @Expose
    var academicopportunities2: String? = null,
    @SerializedName("ell_programs")
    @Expose
    var ellPrograms: String? = null,
    @SerializedName("neighborhood")
    @Expose
    var neighborhood: String? = null,
    @SerializedName("building_code")
    @Expose
    var buildingCode: String? = null,
    @SerializedName("location")
    @Expose
    var location: String? = null,
    @SerializedName("phone_number")
    @Expose
    var phoneNumber: String? = null,
    @SerializedName("fax_number")
    @Expose
    var faxNumber: String? = null,
    @SerializedName("school_email")
    @Expose
    var schoolEmail: String? = null,
    @SerializedName("website")
    @Expose
    var website: String? = null,
    @SerializedName("subway")
    @Expose
    var subway: String? = null,
    @SerializedName("bus")
    @Expose
    var bus: String? = null,
    @SerializedName("grades2018")
    @Expose
    var grades2018: String? = null,
    @SerializedName("finalgrades")
    @Expose
    var finalgrades: String? = null,
    @SerializedName("total_students")
    @Expose
    var totalStudents: String? = null,
    @SerializedName("extracurricular_activities")
    @Expose
    var extracurricularActivities: String? = null,
    @SerializedName("school_sports")
    @Expose
    var schoolSports: String? = null,
    @SerializedName("attendance_rate")
    @Expose
    var attendanceRate: String? = null,
    @SerializedName("pct_stu_enough_variety")
    @Expose
    var pctStuEnoughVariety: String? = null,
    @SerializedName("pct_stu_safe")
    @Expose
    var pctStuSafe: String? = null,
    @SerializedName("school_accessibility_description")
    @Expose
    var schoolAccessibilityDescription: String? = null,
    @SerializedName("directions1")
    @Expose
    var directions1: String? = null,

    //SAT data
    @SerializedName("num_of_sat_test_takers")
    @Expose
    var numOfSatTestTakers: Int? = null,
    @SerializedName("sat_critical_reading_avg_score")
    @Expose
    var satCriticalReadingAvgScore: Int? = null,
    @SerializedName("sat_math_avg_score")
    @Expose
    var satMathAvgScore: Int? = null,
    @SerializedName("sat_writing_avg_score")
    @Expose
    var satWritingAvgScore: Int? = null

)

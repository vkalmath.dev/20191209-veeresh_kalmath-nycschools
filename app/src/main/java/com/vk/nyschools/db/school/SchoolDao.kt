package com.vk.nyschools.db.school

import androidx.paging.DataSource
import androidx.room.*
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.vk.nyschools.model.school.School
import com.vk.nyschools.model.school.SchoolsResponse
import io.reactivex.Observable

@Dao
interface SchoolDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(schools: List<School>)

    @Query("SELECT * FROM schools ORDER BY primary_id")
    fun allSchools(): DataSource.Factory<Int,School>

    @Query("SELECT * FROM schools WHERE primary_id >= :primaryId ORDER BY primary_id")
    fun allSchoolsByPage(primaryId: Int): DataSource.Factory<Int,School>

    @Query("SELECT * FROM schools WHERE (schoolName LIKE '%' || :queryString || '%') " )
    fun listSchoolByName(queryString: String):List<School>

    @Query("SELECT * FROM schools " )
    fun listAllSchools(): List<School>

    @Query("SELECT * FROM schools WHERE dbn = :id")
    fun schoolByDbn(id: String) : Observable<School>

    @Query("SELECT COUNT(*) FROM schools")
    fun schoolsCount() : Int

    @Query("UPDATE schools SET numOfSatTestTakers = :numOfSatTestTakers, " +
            "satCriticalReadingAvgScore = :satCriticalReadingAvgScore, " +
            "satMathAvgScore = :satMathAvgScore, " +
            "satWritingAvgScore = :satWritingAvgScore " +
            "WHERE dbn = :dbn")
    fun updateSatStatForSchool(numOfSatTestTakers: Int,
                               satCriticalReadingAvgScore: Int,
                               satMathAvgScore: Int,
                               satWritingAvgScore: Int,
                               dbn: String
                               )
}

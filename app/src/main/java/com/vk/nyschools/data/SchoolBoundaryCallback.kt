package com.vk.nyschools.data

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.PagedList
import com.vk.nyschools.data.api.SchoolDbService
import com.vk.nyschools.db.school.SchoolDao
import com.vk.nyschools.model.school.School
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

class SchoolBoundaryCallback(
    private val query: String,
    private val service: SchoolDbService,
    private val schoolDao: SchoolDao,
    var compositeDisposable: CompositeDisposable?,
    var currentPage: Int = 1
) : PagedList.BoundaryCallback<School>() {

    private val _networkErrors = MutableLiveData<String>()
    private val _loading = MutableLiveData<Boolean>()

    val networkErrors: LiveData<String>
        get() = _networkErrors

    val loading: LiveData<Boolean>
        get() = _loading

    private var isRequestInProgress = false

    override fun onZeroItemsLoaded() {
        Timber.d("onZeroItemsLoaded")
        requestAndSaveData(query, currentPage)
    }


    override fun onItemAtEndLoaded(itemAtEnd: School) {
        Timber.d("onItemAtEndLoaded")
        requestAndSaveData(query, (itemAtEnd.primary_id!!/ SCHOOL_DATABASE_PAGE_SIZE) +1)
    }


    private fun requestAndSaveData(query: String, nextPage: Int) {
        if (isRequestInProgress) return

        isRequestInProgress = true

        _loading.postValue(true)

        val d = service.getSchools(offset = nextPage)
            .subscribeOn(Schedulers.io())
            .doOnNext{ response ->
                response?.let {
                    schoolDao.insert(it)
                }
            }
            .observeOn(AndroidSchedulers.mainThread())
            .doOnComplete {
                isRequestInProgress = false
                _loading.postValue(false)
            }
            .subscribeWith(object : DisposableObserver<List<School>>() {
                override fun onComplete() {

                }

                override fun onNext(response: List<School>) {

                }

                override fun onError(error: Throwable) {
                    _loading.postValue(false)
                    _networkErrors.postValue(error.localizedMessage)
                    isRequestInProgress = false
                }

            })
        compositeDisposable?.add(d)
    }
}

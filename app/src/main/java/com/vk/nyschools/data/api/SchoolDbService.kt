package com.vk.nyschools.data.api

import com.vk.nyschools.model.school.School
import com.vk.nyschools.model.school.SchoolSAT
import com.vk.nyschools.model.school.SchoolsCount
import com.vk.nyschools.model.school.SchoolsResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query


interface SchoolDbService {

    //base URL: https://data.cityofnewyork.us/

    //https://data.cityofnewyork.us/api/id/s3k6-pzi2.json?$select=`dbn`,`school_name`,`boro`,
    // `overview_paragraph`,`school_10th_seats`,`academicopportunities1`,`academicopportunities2`,
    // `academicopportunities3`,`academicopportunities4`,`academicopportunities5`,`ell_programs`,
    // `language_classes`,`advancedplacement_courses`,`diplomaendorsements`,`neighborhood`,
    // `shared_space`,`campus_name`,`building_code`,`location`,`phone_number`,`fax_number`,
    // `school_email`,`website`,`subway`,`bus`,`grades2018`,`finalgrades`,`total_students`,
    // `start_time`,`end_time`,`addtl_info1`,`extracurricular_activities`,`psal_sports_boys`,
    // `psal_sports_girls`,`psal_sports_coed`,`school_sports`,`graduation_rate`,`attendance_rate`,
    // `pct_stu_enough_variety`,`college_career_rate`,`pct_stu_safe`,`girls`,`boys`,`pbat`,
    // `international`,`specialized`,`transfer`,`ptech`,`earlycollege`,`geoeligibility`,
    // `school_accessibility_description`,`prgdesc1`,`prgdesc2`,`prgdesc3`,`prgdesc4`,
    // \`prgdesc5`,`prgdesc6`,`prgdesc7`,`prgdesc8`,`prgdesc9`,`prgdesc10`,`directions1`,
    // `directions2`,`directions3`&$order=`:id`+ASC&$limit=14&$offset=0
    @GET("api/id/s3k6-pzi2.json?\$order=`:id`+ASC")
    fun getSchools(@Query("\$limit") limit: Int = 10, @Query("\$offset") offset: Int) :
            Observable<List<School>>


    @GET("api/id/s3k6-pzi2.json?\$select=count(*)")
    fun numberOfSchools() : Observable<List<SchoolsCount>>

    @GET("resource/f9bf-2cp4.json")
    fun getSchoolSATDetail(@Query("dbn") dbn: String) : Observable<List<SchoolSAT>>


}

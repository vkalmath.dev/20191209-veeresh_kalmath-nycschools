package com.vk.nyschools.data

import androidx.paging.DataSource
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.vk.nyschools.data.api.SchoolDbService
import com.vk.nyschools.db.school.SchoolDatabase
import com.vk.nyschools.model.school.RepoSchoolResult
import com.vk.nyschools.model.school.School
import com.vk.nyschools.model.school.SchoolSAT
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import timber.log.Timber
import javax.inject.Inject

const val SCHOOL_DATABASE_PAGE_SIZE = 20


class SchoolDbRepository @Inject constructor(
    val schoolDbService: SchoolDbService,
    val schoolDb: SchoolDatabase
) {

    var compositeDisposable: CompositeDisposable? = null

    fun search(query: String, page: Int): RepoSchoolResult {
        Timber.d("Inside search query: ${query}")
        var dataFactory: DataSource.Factory<Int, School>? = null
        //data factory
        if (page == 1) {
            dataFactory = allSchools()
        } else {
            dataFactory = allSchoolsByPage(page)
        }

        //boundary callback
        val boundaryCallback =
            SchoolBoundaryCallback(
                query,
                schoolDbService,
                schoolDb.schoolDao(),
                compositeDisposable,
                page
            )


        //Boundary callback has known open bug for calling onItemEnded multipletimes.
        //https://github.com/android/architecture-components-samples/issues/545
        val config = PagedList.Config.Builder()
//            .setPrefetchDistance(2)
            .setPageSize(SCHOOL_DATABASE_PAGE_SIZE)
            .setEnablePlaceholders(false)
            .build()

        return RepoSchoolResult(
            //LivePagedList Builder
            LivePagedListBuilder(dataFactory, config)
                .setBoundaryCallback(boundaryCallback)
                .build(),
            boundaryCallback.networkErrors,
            boundaryCallback.loading
        )


    }

    fun clear() {
        schoolDb.close()
    }

    fun satByDbn(dbn: String): Observable<List<SchoolSAT>> {
        return schoolDbService.getSchoolSATDetail(dbn)
    }

    fun updateSchoolWithSat(dbn: String, schoolSAT: SchoolSAT) {
        schoolDb.schoolDao().updateSatStatForSchool(
            schoolSAT.numOfSatTestTakers ?: -1,
            schoolSAT.satCriticalReadingAvgScore ?: -1,
            schoolSAT.satMathAvgScore ?: -1,
            schoolSAT.satWritingAvgScore ?: -1,
            dbn
        )
    }

    fun schoolByDbn(dbn: String): Observable<School> {
        return schoolDb.schoolDao().schoolByDbn(dbn)
    }

    //
    fun allSchools(): DataSource.Factory<Int, School> {
        return schoolDb.schoolDao().allSchools()
    }

    //
    fun allSchoolsByPage(page: Int): DataSource.Factory<Int, School> {
        return schoolDb.schoolDao().allSchoolsByPage(page * SCHOOL_DATABASE_PAGE_SIZE)
    }
//
//
//    fun movieById(id: Int): Observable<Movie> {
//        return schoolDb.movieDao().movieById(id)
//    }
}

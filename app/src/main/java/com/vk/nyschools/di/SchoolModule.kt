package com.vk.nyschools.di

import android.content.Context
import androidx.room.Room
import com.vk.nyschools.data.SchoolDbRepository
import com.vk.nyschools.db.school.SchoolDatabase
import com.vk.nyschools.ui.epoxy.school.SchoolListViewModelFactory
import dagger.Module
import dagger.Provides

@Module
class SchoolModule {

    @ActivityScope
    @Provides
    fun provideMovieListViewModelFactory(
        context: Context,
        repository: SchoolDbRepository
    ): SchoolListViewModelFactory {
        return SchoolListViewModelFactory(
            context.applicationContext,
            repository
        )
    }

    @ActivityScope
    @Provides
    fun provideSchoolDatabase(context: Context): SchoolDatabase {
        return Room.databaseBuilder(
            context,
            SchoolDatabase::class.java,
            "schooldb.db"
        ).build()
    }


}

package com.vk.nyschools.di

import android.content.Context
import android.content.SharedPreferences
import com.vk.nyschools.NYSchoolsApplication
import com.vk.nyschools.ui.MainActivity
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [RestModule::class, AppSubComponents::class])
interface AppComponent {

    // Factory to create instances of the AppComponent
    @Component.Factory
    interface Factory {
        // With @BindsInstance, the Context passed in will be available in the graph
        fun create(@BindsInstance context: Context, @BindsInstance application: NYSchoolsApplication,
                   @BindsInstance sharedPrefs: SharedPreferences): AppComponent
    }

    fun schoolListComponent() : SchoolListComponent.Factory

    fun inject(activity: MainActivity)


}

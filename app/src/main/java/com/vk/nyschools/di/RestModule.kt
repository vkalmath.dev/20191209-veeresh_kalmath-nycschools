package com.vk.nyschools.di

import com.vk.nyschools.data.api.SchoolDbService
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Qualifier
import javax.inject.Singleton

const val SCHOOL_API_BASE_URL = "https://data.cityofnewyork.us/"

@Retention(AnnotationRetention.BINARY)
@Qualifier
annotation class SchoolRetrofitInstance

@Retention(AnnotationRetention.BINARY)
@Qualifier
annotation class MovieRetrofitInstance

@Retention(AnnotationRetention.BINARY)
@Qualifier
annotation class SchoolBaseUrl


@Module
class RestModule {

    @Singleton
    @Provides
    fun providesOkHttpClient(): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor(HttpLoggingInterceptor()
                .apply {
                    level = HttpLoggingInterceptor.Level.BODY
                }).build()
    }

    @Singleton
    @Provides
    fun provideSchooldDbService(@SchoolRetrofitInstance retrofit: Retrofit): SchoolDbService {
        return retrofit.create(SchoolDbService::class.java)
    }

    @Singleton
    @SchoolRetrofitInstance
    @Provides
    fun provideSchoolRetrofitInstance(okHttpClient: OkHttpClient, @SchoolBaseUrl baseUrl: String): Retrofit {
        return Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build();
    }

    @Singleton
    @SchoolBaseUrl
    @Provides
    fun provideSchoolBaseUrl() = SCHOOL_API_BASE_URL
}

package com.vk.nyschools.di

import dagger.Module


@Module(subcomponents = [SchoolListComponent::class])
class AppSubComponents

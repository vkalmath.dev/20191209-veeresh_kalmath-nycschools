package com.vk.nyschools.di

import com.vk.nyschools.ui.epoxy.school.SchoolDetailFragment
import com.vk.nyschools.ui.epoxy.school.SchoolListFragment
import dagger.Subcomponent

@ActivityScope
@Subcomponent(modules = [SchoolModule::class])
interface SchoolListComponent {

    @Subcomponent.Factory
    interface Factory {
        fun create() : SchoolListComponent
    }

    fun inject(schoolListFragment: SchoolListFragment)

    fun inject(fragment: SchoolDetailFragment)
}

# NYSchools
Android Take Home project for JPM-CHASE

Design Details:

1) Used Android Architecture Components.
2) Room acts as Single Source of truth. Data is fetched from api and insrted to ROOM db. UI observes to the changes on ROOM. Assumed data does not change so its persisted.
3) SAT data is also part of School Entity, when SAT scores are absent, api is called and School Entity is updated with SAT details, this avoids repeated api call.
3) Used Dagger for Dependency Injection
4) Used combination of RxJava(For Details Screen) and LiveData(For List Screens)
5) Used Navigation Component for fragment navigation.
6) Unit tests added only to the Room Database only and API testing.
7) Used Epoxy for Adapter Delegate for RecyclerView Building.
8) Works both on Tablet(Master->Detail) and phone screen sizes.
9) Works on both Portrait and Landscape mode by retaining the focussed items of the school list.
----------
1) Paging issues are fixed by adding the auto-increment id to the table, which in turn will be used to calculate the next page number.
2) Config changes issues are fixed. Fragment switching issues are fixed.
Faced this knonwn open issue: https://github.com/android/architecture-components-samples/issues/545

Improvements needed:
2) Need to add the filtering mechanism.
3) Other refactoring for retry logic on Errors need to be added.
4) Sizes for dimensnsions nd fond sizes can be refactored instead of hardcoding in XML files and TextView styles can be created.
5) Placeholders for Imageviews can be enabled and populated with shool images, was not able to find with existing apis. left them as place holders
6) Need a strategy to reconcile the data between API and ROOM db. (Could use some timer, after 24 hours the table could be dropped and created again with fresh api data)
